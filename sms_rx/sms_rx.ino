#include <VirtualWire.h>

void setup() {
  Serial.begin(9600);
  vw_setup(2000);
  vw_set_rx_pin(2);
  vw_rx_start();
}

void loop() {
  uint8_t buf;
  uint8_t buflen = 1;

  if (vw_get_message(&buf, &buflen))
      if((char)buf != '_'){
        Serial.print((char)buf);
      }else{
        Serial.println();
      }
  

}
