#define DEBUG 1


#include <VirtualWire.h>
#include <LiquidCrystal.h>
#include <Keypad.h>

#define filas 4
#define cols 4
char teclas[filas][cols] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte pin_filas[] = {23,25,27,29};
byte pin_cols[] = {31,33,35,37};

Keypad teclado = Keypad(makeKeymap(teclas),pin_filas,pin_cols,filas,cols);


LiquidCrystal lcd(43,45,47,49,51,53);

char letras[12][5]{
  {'1','1','1','1','1'},
  {'a','b','c','2',' '},
  {'d','e','f','3',' '},
  {'g','h','i','4',' '},
  {'j','k','l','5',' '},
  {'m','n','o','6',' '},
  {'p','q','r','s','7'},
  {'t','u','v','8',' '},
  {'w','x','y','z','9'},
  {'*',' ',' ',' ',' '},
  {' ',' ',' ','0',' '},
  {'#',' ',' ',' ',' '}
};



String msj = "";
String msj_temp = "";
byte cont =0;
char prev;
bool escriba_un_msj = 0;

void setup(){
  if(DEBUG){    
    Serial.begin(9600);
  }
  vw_setup(2000);
  vw_set_tx_pin(2);
  
  lcd.begin(16,2);
  lcd.clear();
  lcd.cursor();
}

void loop(){
  
  if(msj == "" && msj_temp == "" && !escriba_un_msj){
    lcd.clear();
    lcd.noCursor();
    lcd.setCursor(0,0);
    lcd.print("Escriba un");
    lcd.setCursor(9,1);
    lcd.print("mensaje");
    escriba_un_msj =1;
  }
  char tecla = teclado.getKey();
  if(tecla){
    if(DEBUG){
      Serial.println(tecla);
    }
    if(tecla == prev && tecla != 'A' && tecla != 'D'){
      cont ++;
    }else{
      cont = 0;
      prev = tecla;
      msj = msj_temp;
    }
    cont = cont % 5;
    if(tecla != 'A' && tecla != 'D'){
      
      byte pos;
      switch(tecla){
        case '1':
          pos = 0;
          break;
        
        case '2':
          pos = 1;
          break;
          
        case '3':
          pos = 2;
          break;
          
        case '4':
          pos = 3;
          break;
          
        case '5':
          pos = 4;
          break;
          
        case '6':
          pos = 5;
          break;
          
        case '7':
          pos = 6;
          break;
          
        case '8':
          pos = 7;
          break;
          
        case '9':
          pos = 8;
          break;
          
        case '*':
          pos = 9;
          break;
          
        case '0':
          pos = 10;
          break;
          
        case '#':
          pos = 11;
          break;
      
      }  
      msj_temp = msj + letras[pos][cont];
      if(DEBUG){
        Serial.println("Msj_temp: "+ msj_temp);
      }
      
    }else{
      if(tecla == 'A'){
        int largo = msj.length();
        msj_temp = msj.substring(0,(largo-1));
        msj = msj_temp;
      }else{
        prev = 0;
        cont =0;
        msj_temp = "";
        enviar(msj);
        msj = "";
      }
        
    }
    mostrar_en_pantalla(msj_temp);  
    
  }
    

}

void enviar(String mensaje){
  escriba_un_msj = 0;
  if(DEBUG)
    Serial.println(mensaje);
  lcd.clear();
  lcd.noCursor();
  lcd.setCursor(0,0);
  lcd.print("Enviando");
  //////Opcion 1
  /*
  mensaje += '_';
  char* msj_tx;
  for(int i=0;i<mensaje.length();i++){
    msj_tx[i] = mensaje[i];
  }
  if(DEBUG){
    Serial.print("Msj_tx: ");
    for(int z=0;z<sizeof(msj_tx);z++){
      Serial.print(msj_tx[z]);
    }
  }
  vw_send((uint8_t *)msj_tx,mensaje.length());
  vw_wait_tx();
  */
  ////Opcion 2
  mensaje += "_";
  for(int i=0; i< mensaje.length(); i++){
    char letra[1];
    letra[0] = mensaje[i];
    if(DEBUG)
      Serial.println(letra[0]);
    vw_send((uint8_t*)letra,sizeof(letra));
    vw_wait_tx();
    delay(100);
  }
  //vw_send((uint8_t*)'_',1);
  //vw_wait_tx();
  /////////////
  lcd.clear();
  lcd.print("Enviado");
  delay(500);
}

void mostrar_en_pantalla(String mensaje){
  escriba_un_msj = 0;
  String msj_imp = "";
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0,0);
  lcd.print("Mensaje:");
  lcd.setCursor(0,1);
  if(mensaje.length()>15){
    msj_imp = mensaje.substring(mensaje.length()-16);
  }else{
    msj_imp = mensaje;
  }
  lcd.print(msj_imp);
  if(DEBUG){
    Serial.println("Msj_imp: "+msj_imp);
  }
  
  
}
